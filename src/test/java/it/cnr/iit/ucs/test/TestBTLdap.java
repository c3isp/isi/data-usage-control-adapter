package it.cnr.iit.ucs.test;

import org.junit.Test;

import com.google.gson.Gson;

import iit.cnr.it.ucsinterface.message.tryaccess.TryAccessMessage;
import iit.cnr.it.utility.RESTUtils;
import iit.cnr.it.utility.Utility;

public class TestBTLdap {
  private String message;

  @Test
  public void btPermit() {
    message = Utility.readFileAbsPath(
        "/home/antonio/projects/gitlab/UsageControl/testFiles/BT_Permit.txt");
    TryAccessMessage tryAccessMessage = new Gson().fromJson(message,
        TryAccessMessage.class);
    RESTUtils.asyncPost("http://localhost:8080/tryAccess", tryAccessMessage);
    // FIXME find a way to end tests
    return;
  }
}
