package eu.c3isp.isi.dsaadapter.eventhandler.restapi.types;

import java.util.HashMap;

public class RestResponse {
	private boolean success;
	private HashMap<String, String> additionalProperties = new HashMap<>();

	public RestResponse() {

	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public HashMap<String, String> getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(HashMap<String, String> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

}
