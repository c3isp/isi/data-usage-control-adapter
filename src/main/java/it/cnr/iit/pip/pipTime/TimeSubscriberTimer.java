package it.cnr.iit.pip.pipTime;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;

import iit.cnr.it.ucsinterface.contexthandler.ContextHandlerPIPInterface;
import iit.cnr.it.ucsinterface.message.PART;
import iit.cnr.it.ucsinterface.message.remoteretrieval.MessagePipCh;
import iit.cnr.it.ucsinterface.message.remoteretrieval.PipChContent;
import iit.cnr.it.xacmlutilities.Attribute;
import iit.cnr.it.xacmlutilities.AttributeId;

/**

 * 
 * @author Giacomo Giorgi
 */
public class TimeSubscriberTimer extends TimerTask {

  protected final BlockingQueue<Attribute> subscriptions;
  private ContextHandlerPIPInterface contextHandlerInterface;
  //private final LDAPConnector ldapConnector;
  private final ArrayList<String> attributeNames;

  /**
   * Constructor: it initializes the parameters used by the thread
   * 
   * @param subscriptions_
   *          a reference to the subscriptions map used to read the last
   *          attributes values read
   * @param properties_
   *          a reference to the properties object used to retrieve LDAP sever
   *          authentication parameters
   * @param queue_
   *          a reference to the subscriptions queue used to push changed
   *          attributes
   * 
   */
  public TimeSubscriberTimer(ContextHandlerPIPInterface contextHandlerInterface,
    BlockingQueue<Attribute> subscriptions, ArrayList<String> attributeNames) {
    this.contextHandlerInterface = contextHandlerInterface;
    this.subscriptions = subscriptions;
    this.attributeNames = attributeNames;

  }

  @Override
  public synchronized void run() {


    for (Attribute attribute : subscriptions) {

      //Get the current date to compare with the date given in the policy
      DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
      LocalDateTime now = LocalDateTime.now();  
      
      System.out.println("Current Date " + dtf.format(now));
      
      if (attribute.getAttributeValues(attribute.getAttributeDataType()).get(0)
              .equals(dtf.format(now)))
            ;
      // if the value of the attribute has changed notify the context handler
      else {
        
    	  attribute.setValue(attribute.getAttributeDataType(), dtf.format(now));
        PipChContent pipChContent = new PipChContent();
        pipChContent.addAttribute(attribute);
        MessagePipCh messagePipCh = new MessagePipCh(PART.PIP.toString(),
            PART.CH.toString());
        messagePipCh.setMotivation(pipChContent);
        contextHandlerInterface.attributeChanged(messagePipCh);
      }
    }
    return;
  }

  public void setContextHandlerInterface(
      ContextHandlerPIPInterface contextHandlerInterface) {
    this.contextHandlerInterface = contextHandlerInterface;
  }

  public ContextHandlerPIPInterface getContextHandler() {
    return this.contextHandlerInterface;
  }
}
