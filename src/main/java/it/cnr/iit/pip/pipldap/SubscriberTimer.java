/*
 * CNR - IIT (2015-2016)
 * 
 * @authors Fabio Bindi and Filippo Lauria
 */
package it.cnr.iit.pip.pipldap;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;

import org.springframework.beans.factory.annotation.Value;

import iit.cnr.it.ucsinterface.contexthandler.ContextHandlerPIPInterface;
import iit.cnr.it.ucsinterface.message.PART;
import iit.cnr.it.ucsinterface.message.remoteretrieval.MessagePipCh;
import iit.cnr.it.ucsinterface.message.remoteretrieval.PipChContent;
import iit.cnr.it.xacmlutilities.Attribute;
import iit.cnr.it.xacmlutilities.AttributeId;

/**
 * Thread that monitors mutable attributes for the subscribed users (they are
 * stored in a map with key the serach base used to access their attribuets in
 * the LDAP server and value the list of their Attributes). it perfroms this by
 * doing the following statements for each user: 1) iterates through the
 * subscriprion map to retrieve the last attribute values read 2) Retrieves the
 * value of the mutable attributes in the LDAP server 3) Compares the values
 * obtained at the points 1 and 2 4) If one ore more attributes have changed its
 * value, it creates a JSON string containing these values and the search base
 * of the involved user and push this object in the SubscriptionQueue where the
 * Context Handler will read it
 * 
 * @author Fabio Bindi and Filippo Lauria
 */
public class SubscriberTimer extends TimerTask {

	@Value("${ldap.domainComponent}")
	private String ldapDc;

	protected final BlockingQueue<Attribute> subscriptions;
	private ContextHandlerPIPInterface contextHandlerInterface;
	private final LDAPConnector ldapConnector;
	private final ArrayList<String> attributeNames;

	/**
	 * Constructor: it initializes the parameters used by the thread
	 * 
	 * @param subscriptions_ a reference to the subscriptions map used to read the
	 *                       last attributes values read
	 * @param properties_    a reference to the properties object used to retrieve
	 *                       LDAP sever authentication parameters
	 * @param queue_         a reference to the subscriptions queue used to push
	 *                       changed attributes
	 * 
	 */
	public SubscriberTimer(ContextHandlerPIPInterface contextHandlerInterface, BlockingQueue<Attribute> subscriptions,
			LDAPConnector ldapConnector, ArrayList<String> attributeNames) {
		this.contextHandlerInterface = contextHandlerInterface;
		this.subscriptions = subscriptions;
		this.ldapConnector = ldapConnector;
		this.attributeNames = attributeNames;
	}

	@Override
	public synchronized void run() {
		for (Attribute attribute : subscriptions) {
			// System.out.println("[PipFile] Subscribe iteration");
			String filter = attribute.getAdditionalInformations();

			Map<String, Set<String>> ldapMap = ldapConnector.search(ldapDc, attributeNames, filter);

			Set<String> values = ldapMap.get(new AttributeId(attribute.getAttributeId()).getSplittedAttribute());
			LinkedList<String> oldValues = new LinkedList<>();
			for (List<String> v : attribute.getAttributeValuesMap().values()) {
				oldValues.addAll(v);
			}

			for (String value : values) {
				if (!oldValues.contains(value)) {
					PipChContent pipChContent = new PipChContent();
					pipChContent.addAttribute(attribute);
					MessagePipCh messagePipCh = new MessagePipCh(PART.PIP.toString(), PART.CH.toString());
					messagePipCh.setMotivation(pipChContent);
					contextHandlerInterface.attributeChanged(messagePipCh);
				}
			}

		}
		return;
	}

	public void setContextHandlerInterface(ContextHandlerPIPInterface contextHandlerInterface) {
		this.contextHandlerInterface = contextHandlerInterface;
	}

	public ContextHandlerPIPInterface getContextHandler() {
		return this.contextHandlerInterface;
	}
}
