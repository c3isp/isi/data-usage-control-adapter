package iit.cnr.it.usagecontrolframework.obligationmanager;

import java.lang.reflect.Constructor;
import java.util.List;

import iit.cnr.it.ucsinterface.obligationmanager.ObligationManagerInterface;
import iit.cnr.it.ucsinterface.pip.PIPOMInterface;
import iit.cnr.it.usagecontrolframework.configuration.xmlclasses.XMLObligationManager;

/**
 * This class is in charge of building the obligation manager.
 * <p>
 * </p>
 * <
 * 
 * @author antonio
 *
 */
final public class ObligationManagerBuilder {

	/**
	 * Private constructor to avoid instantiation
	 */
	private ObligationManagerBuilder() {

	}

	/**
	 * TODO comments
	 * 
	 * @param xml
	 * @param pips
	 * @param pipRetrieval
	 * @return
	 */
	final public static ObligationManagerInterface build(XMLObligationManager xml, List<PIPOMInterface> pips,
			PIPOMInterface pipRetrieval) {
		// BEGIN parameter checking
		if (xml == null || (pips.size() == 0 && pipRetrieval == null)) {
			return null;
		}
		// END parameter checking
		try {
			String className = xml.getClassName();
			Constructor<?> constructor = Class.forName(className).getConstructor(XMLObligationManager.class);
			ObligationManagerInterface obligationManagerInterface = (ObligationManagerInterface) constructor
					.newInstance(xml);
			obligationManagerInterface.setPIPs(pips, pipRetrieval);
			if (!obligationManagerInterface.isInitialized()) {

				return null;
			}
			return obligationManagerInterface;

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

}
