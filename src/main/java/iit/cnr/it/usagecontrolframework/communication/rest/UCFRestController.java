package iit.cnr.it.usagecontrolframework.communication.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import iit.cnr.it.ucsinterface.message.endaccess.EndAccessMessage;
import iit.cnr.it.ucsinterface.message.endaccess.EndAccessResponse;
import iit.cnr.it.ucsinterface.message.reevaluation.ReevaluationMessage;
import iit.cnr.it.ucsinterface.message.reevaluation.ReevaluationResponse;
import iit.cnr.it.ucsinterface.message.remoteretrieval.MessagePipCh;
import iit.cnr.it.ucsinterface.message.startaccess.StartAccessMessage;
import iit.cnr.it.ucsinterface.message.startaccess.StartAccessResponse;
import iit.cnr.it.ucsinterface.message.tryaccess.TryAccessMessage;
import iit.cnr.it.ucsinterface.message.tryaccess.TryAccessResponse;
import iit.cnr.it.ucsinterface.node.NodeInterface;
import iit.cnr.it.usagecontrolframework.contexthandler.ContextHandlerLC;
import iit.cnr.it.usagecontrolframework.entry.UsageControlFramework;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * This class includes all the interfaces we will offer via rest.
 * <p>
 * Maybe it is better to offer an interface for each possible request that can
 * be sent to the UCS. In this way we will have many entry points. However all
 * these will be mapped on the same RequestManager function so that it becomes a
 * lot easier to perform task as prioritizing between the queues and so on.
 * </p>
 *
 * @author antonio
 *
 */
@ApiModel(value = "UCSFramework", description = "Usage Control Framework enforcement engine REST API")
@RestController
@RequestMapping("/")
public class UCFRestController {

	@Value("${setting1}")
	private String setting1;

	@Value("${setting2}")
	private String setting2;

	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;

	@Autowired
	private RestTemplate restTemplate;

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
		return restTemplateBuilder.basicAuthentication(restUser, restPassword).build();
	}

	boolean initialized = false;
	private UsageControlFramework usageControlFramework = new UsageControlFramework();

	@ApiOperation(httpMethod = "POST", value = "Receives request from PEP for tryAccess operation")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid message received"),
			@ApiResponse(code = 200, message = "OK") })
	@RequestMapping(method = RequestMethod.POST, value = "/enrichRequest", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> enrichRequest(@RequestBody() TryAccessMessage message) {
		if (message == null) {
			return ResponseEntity.badRequest().build();
		}
		ContextHandlerLC ch = (ContextHandlerLC) usageControlFramework.getContextHandler();
		String requestFull = ch.enrichRequest(message);
		return ResponseEntity.ok(requestFull);
	}
	
	@ApiOperation(httpMethod = "POST", value = "Receives request from PEP for tryAccess operation")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid message received"),
			@ApiResponse(code = 200, message = "OK") })
	@RequestMapping(method = RequestMethod.POST, value = NodeInterface.TRYACCESS_REST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void sendMessage(@RequestBody() TryAccessMessage message) {
		if (message == null) {
			throw new NotFoundException();
		}
		usageControlFramework.tryAccess(message);
	}

	@ApiOperation(httpMethod = "POST", value = "Receives request from PEP for startAccess operation")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid message received"),
			@ApiResponse(code = 200, message = "OK") })
	@RequestMapping(method = RequestMethod.POST, value = NodeInterface.STARTACCESS_REST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void sendMessage(@RequestBody() StartAccessMessage message) {
		if (message == null) {
			throw new NotFoundException();
		}
		System.out.println("[TIME] Startaccess received " + System.currentTimeMillis());
		usageControlFramework.startAccess(message);
	}

	@ApiOperation(httpMethod = "POST", value = "Receives request from PEP for endAccess operation")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid message received"),
			@ApiResponse(code = 200, message = "OK") })
	@RequestMapping(method = RequestMethod.POST, value = NodeInterface.ENDACCESS_REST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void sendMessage(@RequestBody() EndAccessMessage message) {
		if (message == null) {
			throw new NotFoundException();
		}
		System.out.println("[TIME] Endaccess received " + System.currentTimeMillis());
		usageControlFramework.endAccess(message);
	}

	@ApiOperation(httpMethod = "POST", value = "Receives request from PEP for onGoing operation")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid message received"),
			@ApiResponse(code = 200, message = "OK") })
	@RequestMapping(method = RequestMethod.POST, value = NodeInterface.ONGOING_REST, consumes = MediaType.TEXT_PLAIN_VALUE)
	public void sendMessage(@RequestBody() String messageString) {
		if (messageString == null) {
			throw new NotFoundException();
		}
		ReevaluationMessage message = new Gson().fromJson(messageString, ReevaluationMessage.class);
		System.out.println("[TIME] Reevaluation received " + System.currentTimeMillis());
		usageControlFramework.onGoingEvaluation(message);
	}

	@ApiOperation(httpMethod = "POST", value = "Receives request from PIP for attribute retrieval operation")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid message received"),
			@ApiResponse(code = 200, message = "OK") })
	@RequestMapping(method = RequestMethod.POST, value = "/retrieveRemote", consumes = MediaType.TEXT_PLAIN_VALUE)
	public void retrieveRemote(@RequestBody() String message) {
		if (message == null) {
			throw new NotFoundException();
		}
		MessagePipCh messagePipCh = new Gson().fromJson(message, MessagePipCh.class);
		usageControlFramework.retrieveRemote(messagePipCh);
	}

	@ApiOperation(httpMethod = "POST", value = "Receives response from PIP for attribute retrieval operation")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid message received"),
			@ApiResponse(code = 200, message = "OK") })
	@RequestMapping(method = RequestMethod.POST, value = "/retrieveRemoteResponse", consumes = MediaType.TEXT_PLAIN_VALUE)
	public void retrieveRemoteResponse(@RequestBody() String message) {
		if (message == null) {
			throw new NotFoundException();
		}
		// System.out.println("Message arrived via rest: " + message);
		MessagePipCh messagePipCh = new Gson().fromJson(message, MessagePipCh.class);
		usageControlFramework.retrieveRemoteResponse(messagePipCh);
		// usageControlFramework.getRequestManager().sendMessageToCH(messagePipCh);
	}

	@ApiOperation(httpMethod = "POST", value = "handles tryAccess response operation")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid message received"),
			@ApiResponse(code = 200, message = "OK") })
	@RequestMapping(method = RequestMethod.POST, value = NodeInterface.TRYACCESSRESPONSE_REST, consumes = MediaType.TEXT_PLAIN_VALUE)
	public void tryAccessResponse(@RequestBody() String message) {
		if (message == null) {
			throw new NotFoundException();
		}
		TryAccessResponse response = new Gson().fromJson(message, TryAccessResponse.class);
		usageControlFramework.tryAccessResponse(response);
	}

	@ApiOperation(httpMethod = "POST", value = "handles startAccess response operation")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid message received"),
			@ApiResponse(code = 200, message = "OK") })
	@RequestMapping(method = RequestMethod.POST, value = NodeInterface.STARTACCESSRESPONSE_REST, consumes = MediaType.TEXT_PLAIN_VALUE)
	public void startAccessResponse(@RequestBody() String message) {
		if (message == null) {
			throw new NotFoundException();
		}
		StartAccessResponse response = new Gson().fromJson(message, StartAccessResponse.class);
		System.out.println("[TIME] Startaccess received " + System.currentTimeMillis());
		usageControlFramework.startAccessResponse(response);
	}

	@ApiOperation(httpMethod = "POST", value = "handles endAccess response operation")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid message received"),
			@ApiResponse(code = 200, message = "OK") })
	@RequestMapping(method = RequestMethod.POST, value = NodeInterface.ENDACCESSRESPONSE_REST, consumes = MediaType.TEXT_PLAIN_VALUE)
	public void endAccessResponse(@RequestBody() String message) {
		if (message == null) {
			throw new NotFoundException();
		}
		EndAccessResponse response = new Gson().fromJson(message, EndAccessResponse.class);
		System.out.println("[TIME] Endaccess received " + System.currentTimeMillis());
		usageControlFramework.endAccessResponse(response);
	}

	@ApiOperation(httpMethod = "POST", value = "handles onGoing response operation")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid message received"),
			@ApiResponse(code = 200, message = "OK") })
	@RequestMapping(method = RequestMethod.POST, value = NodeInterface.ONGOINGRESPONSE_REST, consumes = MediaType.TEXT_PLAIN_VALUE)
	public void reevaluationResponse(@RequestBody() String message) {
		if (message == null) {
			throw new NotFoundException();
		}
		ReevaluationResponse response = new Gson().fromJson(message, ReevaluationResponse.class);
		System.out.println("[TIME] On going Evaluation received " + System.currentTimeMillis());
		usageControlFramework.onGoingEvaluationResponse(response);
	}
}
