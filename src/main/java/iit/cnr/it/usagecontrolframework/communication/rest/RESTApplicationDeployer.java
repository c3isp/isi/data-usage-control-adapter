package iit.cnr.it.usagecontrolframework.communication.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * This is the deployer for the rest api provided by the usage control framework
 * 
 * @author antonio
 *
 */
@SpringBootApplication
@EnableSwagger2
@EnableAsync
public class RESTApplicationDeployer extends SpringBootServletInitializer {

	@Value("${security.activation.status}")
	private boolean securityActivationStatus;

	/**
	 * Docker is a SwaggerUI configuration component, in particular specifies to use
	 * the V2.0 (SWAGGER_2) of swagger generated interfaces it also tells to include
	 * only path that are under / if other rest interfaces are added with different
	 * base path, they won't be included this path selector can be removed if all
	 * interfaces should be documented.
	 */
	@Bean
	public Docket documentation() {
		Docket docket = new Docket(DocumentationType.SWAGGER_2);
		docket.apiInfo(metadata());
		if (!securityActivationStatus) {
			return docket.select().paths(PathSelectors.regex("/.*")).build();
		} else {
			return docket
					// .securitySchemes(new ArrayList<ApiKey>(Arrays.asList(new
					// ApiKey("mykey", "api_key", "header"))))
					.securitySchemes(new ArrayList<BasicAuth>(Arrays.asList(new BasicAuth("basicAuth"))))
					.securityContexts(new ArrayList<SecurityContext>(Arrays.asList(securityContext()))).select()
					.paths(PathSelectors.regex("/.*")).build();
		}
	}

	/**
	 * it just tells swagger that no special configuration are requested
	 * 
	 */
	@Bean
	public UiConfiguration uiConfig() {
		return new UiConfiguration("validatorUrl");
	}

	/**
	 * the metadata are information visualized in the /basepath/swagger-ui.html
	 * interface, only for documentation
	 */
	private ApiInfo metadata() {
		return new ApiInfoBuilder().title("Usage Control Framework REST API")
				.description("API for Usage Control Framework").version("1.0")
				.contact(new Contact("Antonio La Marra", "", "antonio.lamarra@iit.cnr.it")).build();
	}

	/**
	 * Selector for the paths this security context applies to ("/v1/.*")
	 */
	private SecurityContext securityContext() {
		return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.regex("/.*")).build();
	}

	List<SecurityReference> defaultAuth() {
		AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
		AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
		authorizationScopes[0] = authorizationScope;
		// return new ArrayList<SecurityReference>(Arrays.asList(new
		// SecurityReference("mykey", authorizationScopes)));
		return new ArrayList<SecurityReference>(Arrays.asList(new SecurityReference("basicAuth", authorizationScopes)));
	}

	public static void main(String args[]) {
		SpringApplication.run(RESTApplicationDeployer.class, args);
	}

}
