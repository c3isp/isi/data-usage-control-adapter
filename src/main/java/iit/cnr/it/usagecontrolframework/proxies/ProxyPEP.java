package iit.cnr.it.usagecontrolframework.proxies;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

import eu.c3isp.isi.dsaadapter.eventhandler.restapi.types.RestResponse;
import iit.cnr.it.ucsinterface.constants.CONNECTION;
import iit.cnr.it.ucsinterface.message.Message;
import iit.cnr.it.ucsinterface.message.endaccess.EndAccessResponse;
import iit.cnr.it.ucsinterface.message.reevaluation.ReevaluationResponse;
import iit.cnr.it.ucsinterface.message.startaccess.StartAccessResponse;
import iit.cnr.it.ucsinterface.message.tryaccess.TryAccessResponse;
import iit.cnr.it.ucsinterface.node.NodeInterface;
import iit.cnr.it.ucsinterface.pep.AbstractPEP;
import iit.cnr.it.ucsinterface.pep.PEPInterface;
import iit.cnr.it.ucsinterface.requestmanager.RequestManagerToExternalInterface;
import iit.cnr.it.usagecontrolframework.configuration.xmlclasses.XMLPep;
import iit.cnr.it.utility.RESTUtils;

/**
 * This is the proxy towards the PEP.
 * <p>
 * This proxy is useful because in this way the UCS can assume that everything
 * is local and so call the interfaces provided by the various classes. The only
 * class to modify in the case in which we have a brand new communication link
 * is this one
 * </p>
 * 
 * @author antonio
 *
 */
public class ProxyPEP extends Proxy implements PEPInterface {
	// the type of connection between the proxy and the real PEP
	CONNECTION connection;
	// logger to be used to print messages
	private final Logger LOGGER = Logger.getLogger(ProxyPEP.class.getName());
	// states if the proxy has been initialized correctly
	private boolean initialized = false;
	// --------------------
	// case of a local PEP
	private AbstractPEP abstractPEP;
	// --------------------
	// --------------------
	// case of remote PEP
	// --------------------
	// url of the PEP
	private String url = "";
	// Port on which the PEP is attached to
	private String port = "";
	// interfaces provided by the PEP to allow the proxy to call it when a
	// response is available
	private String tryAccessResponse = "";
	private String startAccessResponse = "";
	private String endAccessResponse = "";
	private String onGoingEvaluation = "";
	private String id = "";

	/**
	 * Constructor for the proxy PEP
	 * 
	 * @param xmlPep the configuration of the PEP in xml format
	 */
	public ProxyPEP(XMLPep xmlPep) {
		// BEGIN parameter checking
		if (xmlPep == null) {
			return;
		}
		String configuration = xmlPep.getCommunication();
		if (configuration == null) {
			return;
		}
		// END parameter checking
		id = xmlPep.getId();

		connection = CONNECTION.getCONNECTION(xmlPep.getCommunication());
		switch (connection) {
		case API:
			if (localPep(xmlPep)) {
				initialized = true;
			}
			break;
		case SOCKET:
			if (connectSocket(xmlPep)) {
				initialized = true;
			}
			break;
		case REST_API:
			if (connectRest(xmlPep)) {
				initialized = true;
			}
			break;
		default:
			LOGGER.log(Level.SEVERE, "WRONG communication " + xmlPep.getCommunication());
			return;
		}
	}

	/**
	 * Function that performs the instantiation of a local PEP
	 * 
	 * @param xmlPep the configuration of the PEP
	 * @return true if everything goes right, false otherwise
	 */
	private boolean localPep(XMLPep xmlPep) {
		String className = xmlPep.getClassName();

		// BEGIN parameter checking
		if (className == null) {
			return false;
		}
		// END parameter checking

		try {
			Constructor<?> constructor = Class.forName(className).getConstructor(XMLPep.class);
			abstractPEP = (AbstractPEP) constructor.newInstance(xmlPep);
			return true;
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | NoSuchMethodException
				| SecurityException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Function that connects to a remote PEP via socket
	 * 
	 * @param xmlPep the configuration of the remote PEP
	 * @return true if everything goes right, false otherwise
	 */
	private boolean connectSocket(XMLPep xmlPep) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Configures all the strings required to connect to a remote PEP via rest
	 * interface
	 * 
	 * @param xmlPe the configuration file for the pep
	 * @return true if everything goes ok, false otherwise
	 */
	private boolean connectRest(XMLPep xmlPep) {
		if ((url = xmlPep.getIp()) == null) {
			LOGGER.log(Level.WARNING, "Missing parameter in configuration file: " + url);
			return false;
		}

		if ((port = xmlPep.getPort()) == null) {
			LOGGER.log(Level.WARNING, "Missing parameter in configuration file: " + url);
			return false;
		}

		if ((onGoingEvaluation = xmlPep.getOnGoingEvaluation()) == null) {
			LOGGER.log(Level.WARNING, "Missing parameter in configuration file: " + url);
			return false;
		}

		if ((tryAccessResponse = xmlPep.getTryAccessResponse()) == null) {
			LOGGER.log(Level.WARNING, "Missing parameter in configuration file: " + url);
			return false;
		}

		if ((startAccessResponse = xmlPep.getStartAccessResponse()) == null) {
			LOGGER.log(Level.WARNING, "Missing parameter in configuration file: " + url);
			return false;
		}

		if ((endAccessResponse = xmlPep.getEndAccessResponse()) == null) {
			LOGGER.log(Level.WARNING, "Missing parameter in configuration file: " + url);
			return false;
		}
		return true;
	}

	public boolean isInitialized() {
		return initialized;
	}

	@Override
	public boolean ping() {
		return true;
	}

	/**
	 * In the case of a local PEP sets its the request manager interface it has to
	 * use
	 * 
	 * @param requestManager the request manager the PEP has to use to communicate
	 *                       with the Request Manager
	 */
	public void setRequestManagerInterface(RequestManagerToExternalInterface requestManager) {
		switch (connection) {
		case API:
			abstractPEP.setRequestManagerInterface(requestManager);
			break;
		default:
			break;
		}

	}

	@Override
	public Message onGoingEvaluation(Message message) {
		switch (connection) {
		case API:
			return abstractPEP.onGoingEvaluation(message);
		case REST_API:
			/*
			 * String response = new Gson().toJson((ReevaluationResponse) message);
			 * NotificationSessionRequest notificationSessionRequest = new
			 * NotificationSessionRequest();
			 * notificationSessionRequest.setEventType(EventTypes.ON_REVOKE);
			 * notificationSessionRequest.setUrl(message.getDestination());
			 * notificationSessionRequest.putProperty("message", response); RESTUtils.post(
			 * "https://isic3isp.iit.cnr.it:8443/event-handler/notifyEvent",
			 * notificationSessionRequest);
			 */
			RESTUtils.asyncPostAsString(buildUrl(NodeInterface.ONGOINGRESPONSE_REST), (ReevaluationResponse) message);
			break;
		default:
			break;
		}
		return null;
	}

	@Override
	public void receiveResponse(Message message) {
		switch (connection) {
		case API:
			abstractPEP.receiveResponse(message);
			break;
		/**
		 * LONG STORY:
		 * <p>
		 * Here we have two possibilities: one is to set the interfaces to be used in
		 * the configuration file, the other is to let the PEP write inside the message
		 * which is the name of the interface it wants the request manager to call. Both
		 * possibilities have pros and cons
		 * </p>
		 */
		case REST_API:
			String toBeSent = "";
			String eventType = "";
			if (message instanceof TryAccessResponse) {
				/*
				 * TryAccessResponse response = (TryAccessResponse) message; toBeSent = new
				 * Gson().toJson(response); eventType = EventTypes.TRY_ACCESS_RESPONSE1;
				 */
				// RESTUtils.asyncPostAsString(buildUrl(tryAccessResponse),
				// (TryAccessResponse) message);
				RestResponse restResponse = RESTUtils.postAsString(buildUrl(tryAccessResponse),
						(TryAccessResponse) message, RestResponse.class);
				LOGGER.info("Response: " + restResponse.isSuccess());
			}
			if (message instanceof StartAccessResponse) {
				/*
				 * StartAccessResponse response = (StartAccessResponse) message; toBeSent = new
				 * Gson().toJson(response); eventType = EventTypes.START_ACCESS_RESPONSE;
				 */
				RESTUtils.asyncPostAsString(buildUrl(startAccessResponse), (StartAccessResponse) message);
			}
			if (message instanceof EndAccessResponse) {
				/*
				 * EndAccessResponse response = (EndAccessResponse) message; toBeSent = new
				 * Gson().toJson(response); eventType = EventTypes.END_ACCESS_RESPONSE;
				 */
				RESTUtils.asyncPostAsString(buildUrl(endAccessResponse), (EndAccessResponse) message);
			}
			break;
		/*
		 * NotificationSessionRequest notificationSessionRequest = new
		 * NotificationSessionRequest();
		 * notificationSessionRequest.setEventType(eventType);
		 * notificationSessionRequest.setUrl(message.getDestination());
		 * notificationSessionRequest.putProperty("message", toBeSent); RESTUtils.post(
		 * "https://isic3isp.iit.cnr.it:8443/event-handler/notifyEvent",
		 * notificationSessionRequest); break;
		 */
		default:
			LOGGER.log(Level.SEVERE, "Error in the receive response");
			break;
		}

	}

	/**
	 * Function to start the local PEP
	 */
	public void start() {
		switch (connection) {
		case API:
			try {
				abstractPEP.start();
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		default:
			break;
		}
	}

	/**
	 * Provided the name of the rest API to call, it builds up the complete url to
	 * be used to call that interface
	 * 
	 * @param function the name of the function
	 * @return the complete url to be used in the rest call
	 */
	private String buildUrl(String function) {
		StringBuilder url = new StringBuilder();
		url.append(this.url);
		url.append("/");
		url.append(function);
		return url.toString();
	}

	public String getURL() {
		return url;
	}

	public String getPort() {
		return port;
	}
}
