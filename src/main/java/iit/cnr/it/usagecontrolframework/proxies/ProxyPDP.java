package iit.cnr.it.usagecontrolframework.proxies;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

import iit.cnr.it.ucsinterface.constants.CONNECTION;
import iit.cnr.it.ucsinterface.contexthandler.STATUS;
import iit.cnr.it.ucsinterface.pdp.AbstractPDP;
import iit.cnr.it.ucsinterface.pdp.PDPEvaluation;
import iit.cnr.it.ucsinterface.pdp.PDPInterface;
import iit.cnr.it.usagecontrolframework.configuration.xmlclasses.XMLPdp;

/**
 * This is the class implementing the proxy towards the PDP.
 * <p>
 * 
 * </p>
 * 
 * @author antonio
 *
 */
final public class ProxyPDP extends Proxy implements PDPInterface {
	// effective implementation of the PDP
	AbstractPDP abstractPDP;
	// volatile variable to avoid wrong initialization
	private volatile boolean initialized = false;
	// logger object
	private static final Logger LOGGER = Logger.getLogger(ProxyPDP.class.getName());
	private String configuration;

	/**
	 * This is the constructor of the proxy to the PDP.
	 * <p>
	 * Basically here we have to distinguish between the various possibilities we
	 * have to implement the pdp and act accordingly. There are 3 different options:
	 * <ol>
	 * <li>through API: in this case this means that the PDP is in the same virtual
	 * machine of the UCS</li>
	 * <li>through REST API: this means that the PDP can be queried using API
	 * offered via REST</li>
	 * <li>through SOCKET: this means that the PDP can be queried by passing to it
	 * messages via sockets</li>
	 * </ol>
	 * </p>
	 * 
	 * @param xmlPdp the configuration of the PDP in xml form
	 */
	public ProxyPDP(XMLPdp xmlPdp) {

		// BEGIN parameter checking
		if (xmlPdp == null) {
			return;
		}
		configuration = xmlPdp.getCommunication();
		if (configuration == null) {
			return;
		}
		// END parameter checking

		CONNECTION connection = CONNECTION.getCONNECTION(xmlPdp.getCommunication());
		switch (connection) {
		case API:
			if (localPdp(xmlPdp)) {
				initialized = true;
			}
			break;
		case SOCKET:
			if (connectSocket(xmlPdp)) {
				initialized = true;
			}
			break;
		case REST_API:
			if (connectRest(xmlPdp)) {
				initialized = true;
			}
			break;
		default:
			LOGGER.log(Level.SEVERE, "WRONG communication " + xmlPdp.getCommunication());
			return;
		}
	}

	private boolean connectSocket(XMLPdp xmlPdp) {
		// TODO Auto-generated method stub
		return false;
	}

	private boolean connectRest(XMLPdp xmlPdp) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Initialization of the localPDP
	 * 
	 * @param xmlPdp the xml configuration of the local pdp
	 * @return true if everything goes ok, false otherwise
	 */
	private boolean localPdp(XMLPdp xmlPdp) {
		String className = xmlPdp.getClassName();

		// BEGIN parameter checking
		if (className == null) {
			return false;
		}
		// END parameter checking

		try {
			Constructor<?> constructor = Class.forName(className).getConstructor(XMLPdp.class);
			abstractPDP = (AbstractPDP) constructor.newInstance(xmlPdp);
			return true;
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | NoSuchMethodException
				| SecurityException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public PDPEvaluation evaluate(String request, String policy) {
		if (initialized == true) {
			return abstractPDP.evaluate(request, policy);
		}
		return null;
	}

	@Override
	public PDPEvaluation evaluate(String request) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isInitialized() {
		return initialized;
	}

	/**
	 * Sets the interfaces the PDP needs to deal with the ObligationManager and the
	 * PAP
	 * 
	 * @param proxyPAP the PAP
	 */
	public void setInterfaces(ProxyPAP proxyPAP) {
		// BEGIN parameter checking
		if (proxyPAP == null) {
			return;
		}
		// END parameter checking
		if ((CONNECTION.getCONNECTION(configuration) == CONNECTION.API) && abstractPDP.isInitialized()) {
			abstractPDP.setPAPInterface(proxyPAP);
			initialized = true;
		}
	}

	@Override
	public boolean ping() {
		if (initialized) {
			LOGGER.log(Level.INFO, "PDPProxy correctly configured");
			return true;
		} else {
			LOGGER.log(Level.SEVERE, "PDPProxy wrongly configured");
			return false;
		}
	}

	@Override
	public PDPEvaluation evaluate(String request, StringBuilder policy, STATUS status) {
		if (initialized == true) {
			return abstractPDP.evaluate(request, policy, status);
		}
		return null;
	}

}
