package iit.cnr.it.usagecontrolframework.contexthandler.exceptions;

public class SessionManagerException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SessionManagerException(String string) {
		super(string);
	}
}
