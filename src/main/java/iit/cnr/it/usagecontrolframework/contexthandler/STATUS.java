package iit.cnr.it.usagecontrolframework.contexthandler;

public enum STATUS {
	TRYACCESS, STARTACCESS, ENDACCESS, REVOKE, REEVALUATION
}
