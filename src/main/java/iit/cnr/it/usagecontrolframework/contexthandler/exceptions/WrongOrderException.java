package iit.cnr.it.usagecontrolframework.contexthandler.exceptions;

public class WrongOrderException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WrongOrderException(String string) {
		super(string);
	}
}
