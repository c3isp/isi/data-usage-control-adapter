package iit.cnr.it.usagecontrolframework.contexthandler.exceptions;

public class RevokeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RevokeException(String string) {
		super(string);
	}
}
